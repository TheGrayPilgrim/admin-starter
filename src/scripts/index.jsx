import React from 'react'
import { render } from 'react-dom'
import { Router, Route, IndexRoute, browserHistory } from 'react-router'
import App from './modules/app.jsx'
import Home from './modules/home.jsx'
import Page1 from './modules/page1.jsx'
import Page2 from './modules/page2.jsx'
import Page1Home from './modules/page1-home.jsx'
import Tab1 from './modules/tab1.jsx'
import Tab2 from './modules/tab2.jsx'

render((
  <Router history={browserHistory}>
    <Route path="/" component={App}>
      <IndexRoute component={Home} />
      <Route path="page-1" component={Page1}>
        <IndexRoute component={Page1Home} />
        <Route path="tab-1" component={Tab1} />
        <Route path="tab-2" component={Tab2} />        
      </Route>
      <Route path="page-2" component={Page2} />      
    </Route>
  </Router>
), document.getElementById('app'))
