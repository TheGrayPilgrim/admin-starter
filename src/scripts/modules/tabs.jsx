import React from 'react'

export default React.createClass({
  render() {
    let children = React.Children.map(this.props.children, (child, index) => {
      return (
        <li key="{ index }">
          { child }
        </li>
      )
    })
      
    return (
      <nav className="tabs">
        <ul>{ children }</ul>
      </nav>
    )
  }
})
