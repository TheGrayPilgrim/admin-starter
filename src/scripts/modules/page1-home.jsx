import React from 'react'
import DocumentTitle from 'react-document-title'

export default React.createClass({
  render() {
    return (
      <DocumentTitle title="Dashboard | Page 1">
        <section className="main_content">
          <p>Page 1 Dashboard</p>
        </section>
      </DocumentTitle>
    )
  }
})
