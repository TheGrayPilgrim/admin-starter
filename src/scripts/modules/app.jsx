import React from 'react'
import DocumentTitle from 'react-document-title'
import NavLink from './navlink.jsx'

export default React.createClass({
  render() {
    return (
      <section>
        <header>
          <nav>
            <ul>
              <li>
                <NavLink to="/" onlyActiveOnIndex>Home</NavLink>
              </li>
              <li>
                <NavLink to="/page-1">Page 1</NavLink>
              </li>
              <li>
                <NavLink to="/page-2">Page 2</NavLink>
              </li>
            </ul>
          </nav>
        </header>
        
        {this.props.children}

        <footer>
          Common Footer
        </footer>
      </section>
    )
  }
})
