import React from 'react'
import DocumentTitle from 'react-document-title'
import NavLink from './navlink.jsx'
import Tabs from './tabs.jsx'

export default React.createClass({
  render() {
    return (
      <main>
        <Tabs>
          <NavLink to="/page-1/" onlyActiveOnIndex>Dashboard</NavLink>
          <NavLink to="/page-1/tab-1">First Tab</NavLink>
          <NavLink to="/page-1/tab-2">Second Tab</NavLink>
        </Tabs>

        { this.props.children }
      </main>
    )
  }
})
